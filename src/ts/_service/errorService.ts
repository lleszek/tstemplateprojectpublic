import { app } from "../_libs/_appLib";

export type errorServiceT = {
	onError: Function;
	lastError: {};
	getLastError: Function;
	init: Function;
};

app.addService(errorService);

function errorService(): errorServiceT {
	let lastError: {
		event?: Event | string;
		source?: string;
		lineno?: number;
		colno?: number;
		error?: Error;
	} = {};

	let exports: errorServiceT = {
		onError: onError,
		lastError: lastError,
		getLastError: Function,
		init: init,
	};

	function onError(e: ErrorEvent) {
		alert(
			e.message + "\n" + (e.error.stack !== undefined)
				? e.error.stack
				: ""
		);
		//throw e;
		// return true;
	}

	function getLastError() {
		return lastError;
	}

	function init() {
		(<any>window).addEventListener(
			"error",
			(event: ErrorEvent): boolean => {
				if (event.message) {
					onError(event);
					return true;
				}
				return false;
			}
		);

		window.onerror = (
			event: string | Event,
			source?: string | undefined,
			lineno?: number | undefined,
			colno?: number | undefined,
			error?: Error | undefined
		): boolean => {
			if (error !== undefined) {
				onError(event as ErrorEvent);
				//console.log(error, { type: "Info" });
				return true;
			} else {
				throw event;
				return false;
			}
		};
		return false;
	}

	return exports;
}
