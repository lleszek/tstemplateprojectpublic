import { app } from "../_libs/_appLib";

app.addService(setupService);

export type dbConfig = {
	url: string;
	user: string;
	token: string;
	projectId: string;
};
export type setupServiceT = {
	dbConfig: dbConfig;
	appName: string;
	appVersion: number;
};

function setupService(): setupServiceT {
	let exports: setupServiceT = {
		appName: "Nasza cudowna aplikacja",
		appVersion: 1.0,
		dbConfig: {
			url: "https://jakasbaza.com",
			user: "Ferdynand",
			token: "SNJWNDBDJKBWJKDbj83898494hfjnjknfkjnjekn",
			projectId: "FGHGSHGSH18883",
		},
	};

	return exports;
}
