import { app } from "../_libs/_appLib";

app.addService(userService);

export interface User {
	name: string;
	isLogged: boolean;
}
export type userServiceT = {
	getUser: () => User;
	loginUser: () => void;
};

function userService(): userServiceT {
	let userName = "Anonim";
	let isLogged = false;

	let exports: userServiceT = {
		getUser: getUser,
		loginUser: loginUser,
	};

	function getUser() {
		return { name: userName, isLogged: isLogged };
	}

	function loginUser(): void {
		isLogged = true;
		app.broadcast("userAuthChanged");
	}

	setTimeout(() => {
		app.broadcast("userAuthChanged");
	}, 2000);

	return exports;
}
