import { app, Controller } from "../../_libs/_appLib";
import { errorServiceT } from "../../_service/errorService";
import { User, userServiceT } from "../../_service/userService";

type windyModelT = {
	user: User;
	appName: string;
};

app.addController(windyViewController);

function windyViewController(
	errorService: errorServiceT,
	userService: userServiceT
): Controller {
	"use strict";
	type windaT = {
		[index: string]: string | boolean;
		"nazwa": string;
		"pietra": string;
		"towarowa": boolean;
	};

	let errServ = errorService;

	let windy: windaT[] = [
		{ "nazwa": "W1", "pietra": "1-5", "towarowa": false },
		{ "nazwa": "W2", "pietra": "10-15", "towarowa": false },
		{ "nazwa": "W3", "pietra": "15-25", "towarowa": false },
		{ "nazwa": "W1", "pietra": "1-15", "towarowa": true },
	];

	let osoba: {};

	var exports = {
		name: "windaViewController",
		model: {
			windy: windy,
			appName: "Nasza super Apka",
		},
		onModelChange: function onModelChange() {},
		refreshModel: refreshModel,
		clickAddWinda: clickAddWinda,
		deleteWinda: deleteWinda,
	};

	async function refreshModel() {
		exports.onModelChange();
	}

	function clickAddWinda() {
		//exports.onModelChange();
		let winda: windaT = { "nazwa": "", "pietra": "", "towarowa": false };
		let form: HTMLFormElement = document.getElementsByTagName("form")[0];
		Array.from(form.getElementsByTagName("input")).forEach((element) => {
			let pole = element.name as keyof windaT;
			let val =
				element.type == "checkbox" ? element.checked : element.value;
			winda[pole] = val;
		});
		exports.model.windy.push(winda);
		exports.onModelChange();
	}

	let pies: { nazwa: string; ilelapzdr: number; adres?: string };

	function deleteWinda(e: MouseEvent) {
		let poz = parseInt((e.currentTarget as HTMLDivElement).innerText);
		exports.model.windy.splice(poz, 1);
		exports.onModelChange();
	}

	return exports;
}
