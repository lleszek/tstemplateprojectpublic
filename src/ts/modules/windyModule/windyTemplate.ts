import { app } from "../../_libs/_appLib";
import windyHtml from "./html/windy.html";
import formAddWinda from "./html/formAddWinda.html";
import windyFragm from "./html/windyFragm.html";

let windyTemplate = windyHtml
	.replace("#formaddwinda#", formAddWinda)
	.replace("#windyfragm#", windyFragm);

app.addTemplate("windyTemplate", windyTemplate);
