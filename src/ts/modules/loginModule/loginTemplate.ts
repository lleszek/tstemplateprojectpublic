import { app } from "../../_libs/_appLib";

import loginHtml from "./html/login.html";
import panelHtml from "./html/panelFragm.html";

let loginTemplate = loginHtml.replace("<panelfrag />", panelHtml);

app.addTemplate("loginTemplate", loginTemplate);
