import { app, Controller } from "../../_libs/_appLib";
import { errorServiceT } from "../../_service/errorService";
import { User, userServiceT } from "../../_service/userService";
import "../windyModule/windyModule";

app.addController(loginViewController);

export function loginViewController(
	errorService: errorServiceT,
	userService: userServiceT
): Controller {
	"use strict";

	let exports = {
		name: "loginViewController",
		model: {
			isReady: true,
			user: {
				isLogged: false,
				name: "",
			},
		},
		clickLogin: clickLogin,
		onModelChange: function onModelChange() {},
		refreshModel: function refreshModel() {},
		clickStart: clickStart,
	};
	let model = exports.model;

	function updateUser(): void {
		if (userService.getUser().isLogged) {
			setUser();
		} else {
			clearUser();
		}
		exports.onModelChange();
	}

	function clearUser() {
		model.user = {
			isLogged: false,
			name: "",
		};
	}

	function setUser() {
		let user = userService.getUser();

		model.user = user;
		model.isReady = true;
	}

	function clickLogin() {
		userService.loginUser();
	}

	function clickStart() {
		app.$router.showRoute("windy");
	}

	app.subscribe("userAuthChanged", updateUser);

	return exports;
}
