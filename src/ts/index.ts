import { app, handlebars, ViewT, ParamsT, RouteT } from "./_libs/_appLib";
import "./_service/all";
import "./modules/loginModule/loginModule.ts";
import "./modules/windyModule/windyModule.ts";
import "./modules/navModule/navModule.ts";

document.addEventListener("DOMContentLoaded", function () {
	"use strict";
	app.$router.globalViews = [
		{
			name: "footerMainView",
			params: {
				global: true,
				renderElem: "footer",
				template: "navTemplate",
				controllerName: "navController",
			},
		},
	];

	app.$router.routes = {
		"#": {
			name: "#",
			nav: false,
			footer: true,
			views: [
				{
					name: "loginView",
					params: {
						isAutoShow: false,
						renderElem: "[data-role=page]",
						template: "loginTemplate",
						controllerName: "loginViewController",
					},
				},
			],
		},
		"windy": {
			name: "windy",
			nav: false,
			footer: true,
			views: [
				{
					name: "windyView",
					params: {
						isAutoShow: false,
						renderElem: "[data-role=page]",
						template: "windyTemplate",
						controllerName: "windyViewController",
					},
				},
			],
		},
	};

	app.start("#");

	window.addEventListener("hashchange", function () {
		app.broadcast("onHashChange", [location.hash]);
	});
	function handleVisibilityChange() {
		if (document.hidden) {
		} else {
			app.broadcast("onAppShow");
		}
	}

	document.addEventListener(
		"visibilitychange",
		handleVisibilityChange,
		false
	);

	//Helpers in handlebars
	/* handlebars.Handlebars.registerHelper(
    "equal",
    function (v1: any, v2: any, options: any) {
      if (v1 === v2) {
        return options.fn(this);
      }
      return options.inverse(this);
    }
  );*/
});
