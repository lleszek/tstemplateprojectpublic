//publiczne synonimy funkcji document.querySelector oraz document.querySelectorAll

import * as Handlebars from "./handlebars";
import html from "*html";

export let handlebars = Handlebars;

/*(<any>window)._$ = function (param: string) {
  return document.querySelector(param as string);
};*/

/*(<any>Object).prototype._$ = function (this: Object, param: string) {
  return (this as Element).querySelector(param);
};*/

/*(<any>Object).prototype._$All = function (this: Object, param: string) {
  return (this as Element).querySelectorAll(param);
};*/

export type ViewT = {
	name: string;
	params: ParamsT;
};

export type RouteT = {
	name: string;
	nav: boolean;
	footer: boolean;
	views: ViewT[];
};

export interface Controller {
	name: string;
	model?: {};
	onRouteChange?: Function;
	refreshModel?: Function;
	onModelChange?: Function;
}

export type ParamsT = {
	global?: boolean;
	subViews?: ViewT[];
	renderElem: string;
	route?: string;
	isAutoShow?: boolean;
	controllerName: string;
	controller?: Function;
	template: string;
};

/**
 * [[Obiekt cache'u obiektów aplikacji: service i controllers. Przechowuje skompilowane wersje.
 *   Jeśli dany obiekt nie istnieje w cache zostanie skompilowany i dodany do cache
 *   kompilacja uwzglednia zależności tj. jeśli controller używa jakiegoś service to najpierw zostanie
 *   pobrany skompilowany service któy może zależeć od innego service itd...]]
 *@author Leszek Lezoń
 */
class _moduleCache {
	_cache: { [key: string]: object } = {};
	_services: { [key: string]: Function };
	_controllers: { [key: string]: Function };

	constructor(
		services: { [key: string]: Function },
		controllers: { [key: string]: Function }
	) {
		this._services = services;
		this._controllers = controllers;
	}

	get(name: string, locals?: { [key: string]: {} }): object {
		if (this._cache[name]) {
			return this._cache[name];
		}
		var provider = this._services[name];
		if (!provider) {
			provider = this._controllers[name];
			if (!provider) {
				throw "Brak definicji service lub kontrolera:" + name;
			}
		}
		return (this._cache[name] = this.compile(provider, locals));
	}

	paramsArr(fn: Function): string[] {
		var res = fn
			.toString()
			.replace(/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm, "")
			.match(/\((.*?)\)/);
		if (res && res[1]) {
			return res[1].split(",").map(function (d) {
				return d.trim();
			});
		}
		return [];
	}

	compile(fn: Function, locals?: { [key: string]: {} }): object {
		locals = locals || {};
		let _this = this;

		let deps = this.paramsArr(fn).map(function (s: string) {
			return locals![s] || _this.get(s, locals);
		}, this);
		return fn.apply(null, deps);
	}
} //end _moduleCache

class appLib {
	//prywatne zmienne i funkcje
	listeners: { [key: string]: Function[] } = {};
	controllers: { [key: string]: Function } = {};
	services: { [key: string]: Function } = {};
	templates: { [key: string]: string } = {};
	routeObservers: Controller[] = [];
	$router: Router = new Router(this);
	_moduleCache: _moduleCache = new _moduleCache(
		this.services,
		this.controllers
	);

	/**
	 * [[Tworzy widok i go wyświetla w trybie asynchronicznym]]
	 * @author Leszek
	 * @param {[[Type]]} name  [[Description]]
	 * @param {object}   params [[Description]]
	 */
	createView(name: string, currRoute: string, params: ParamsT): void {
		params.route = currRoute;
		const view: View = new View(name, params);
		if (view.isAutoShow) {
			//setTimeout(function () {
			let contr = this.getController(params.controllerName);
			view.show(view.renderElem, currRoute);

			//view.addListeners();
			if (view.subViews && view.subViews.length > 0) {
				view.subViews.forEach((subview) => {
					subview.params.route = currRoute;
					this.createView(subview.name, currRoute, subview.params);
				});
			}
			// }, params.timeout || 0);
		} else {
			let contr = this.getController(params.controllerName);
			if (contr.refreshModel) {
				contr.refreshModel();
			} else {
				throw (
					"View: " +
					name +
					" wymaga w controller:" +
					params.controllerName +
					" istnienia funkcji: refreshModel"
				);
			}
		}
	}

	/**
	 * [[Description]]
	 * @author Leszek
	 * @param   {object}   param [[Description]]
	 * @returns {[[Type]]} [[Description]]
	 */
	getTemplate(name: string): string {
		if (name !== undefined) {
			var template = this.templates[name];
			if (template === undefined) {
				throw "Brak definicji template:" + name;
			}
			return template;
		}
		throw "Brak nazwy template" + name;
	}

	getController(name: string): Controller {
		if (name !== undefined && name !== "") {
			let controller = this._moduleCache.get(name) as Controller;
			if (controller === undefined) {
				throw "Brak definicji kontrolera:" + name;
			}
			if (controller.onRouteChange) {
				var observer = false;
				this.routeObservers.forEach(function (elem) {
					if (elem === controller) {
						observer = true;
					}
				});
				if (!observer) {
					this.routeObservers.push(controller);
				}
			}
			return controller;
		}

		throw "Nie podano kontrolera:";
	}

	onRouteChange = () => {
		this.routeObservers.forEach((elem: Controller) => {
			elem.onRouteChange!();
		});
	};

	//publiczne zmienne i funkcje
	addTemplate(name: string, template: string): void {
		this.templates[name] = template;
	}

	subscribe(topic: string, listener: Function): void {
		if (!this.listeners[topic]) {
			this.listeners[topic] = [];
		}
		this.listeners[topic].push(listener);
	}

	unsubscribe(topic: string, listener: Function): void {
		if (!this.listeners[topic]) {
			return;
		}
		this.listeners[topic] = this.listeners[topic].filter((elem) => {
			if (elem == listener) {
				return false;
			} else {
				return true;
			}
		});
	}

	broadcast(topic: string, args?: object): void {
		if (!this.listeners[topic]) {
			return;
		}
		this.listeners[topic].forEach((elem) => {
			elem(args);
		});
	}

	addService(service: Function): void {
		this.services[service.name] = service;
	}

	addController(controller: Function): void {
		this.controllers[controller.name] = controller;
	}

	//start aplikacji
	start(page: string): void {
		this.$router.init(page);
	}
} ///end appLib

/**
 * [[Class'a construktor obiektu View]]
 * @author Leszek
 * @returns {[[Type]]} [[Description]]
 */
class View {
	constructor(name: string, params: ParamsT) {
		this.name = name;
		this.params = params;
		this.subViews =
			this.params.subViews == undefined ? [] : this.params.subViews;
		this.isGlobal =
			this.params.global == undefined ? false : this.params.global;
		this.renderElem =
			this.params.renderElem == undefined
				? "[data-role=view]"
				: this.params.renderElem;
		this.route = this.params.route == undefined ? "#" : this.params.route;
		this.isAutoShow =
			this.params.isAutoShow == undefined ? true : this.params.isAutoShow;
		this.template = app.getTemplate(this.params.template);
		this.controllerName = this.params.controllerName;
		this.controller = this.controllerName
			? app.getController(this.controllerName)
			: undefined;
		this.model =
			this.controller !== undefined ? this.controller.model! : {};

		if (this.controller && this.controller.onModelChange !== undefined) {
			this.controller.onModelChange = () => {
				this.model = this.controller!.model || {};
				this.show(this.renderElem, app.$router.currentRoute!);
			};
		}
	}

	_$ = (<any>window)._$;
	name: string;
	params: ParamsT;
	private actions: string[] = [
		"click",
		"tap",
		"swipe-left",
		"swipe-rigth",
		"scroll",
		"animationend",
		"change",
		"keyup",
		"mousedown",
		"touchstart",
	];
	isGlobal: boolean;
	subViews?: ViewT[];
	renderElem: string;
	route: string;
	template: string;
	isAutoShow: boolean;
	controller?: Controller;
	controllerName?: string;
	model: {} = {};

	//var route = app.$router.currentRoute;

	//methods

	/**
	 * [[Renderuje widok i go wyświetla wraz z podwidokami]]
	 * @author Leszek
	 * @param {[[string]]} name  [[string]]
	 * @param {Params}   param [[Params]]
	 */
	show(elem: string, currentRoute: string): void {
		//self.model = self.controller.model||{};
		if (this.route != currentRoute && !this.isGlobal) {
			return;
		}
		if (
			document.querySelector(elem) == undefined &&
			document.querySelector(this.renderElem) == undefined
		) {
			throw (
				"Brak elementu: " +
				(elem || this.renderElem) +
				" do wyswietlenia view:" +
				this.name
			);
		}
		document.querySelector(elem || this.renderElem)!.innerHTML =
			this.render(this.template)(this.model);
		this.addListeners();
		this.showSubViews(this.subViews!, currentRoute);
	}

	showSubViews(
		views: { name: string; params: ParamsT }[],
		currRoute: string
	): void {
		if (views && views.length > 0) {
			views.forEach((subview: { name: string; params: ParamsT }) => {
				let view = new View(subview.name, subview.params);
				view.show(view.renderElem, currRoute);
			});
		}
	}

	addListeners(): void {
		this.actions.forEach((action: string) => {
			document
				.querySelector(this.renderElem)!
				.querySelectorAll("[data-" + action + "]")
				.forEach((elem: Element) => {
					let contrFun = elem.getAttribute("data-" + action);
					if (this.controller) {
						let fn = this.controller[
							contrFun as keyof Controller
						] as EventListenerOrEventListenerObject;
						if (fn == undefined) {
							throw (
								"Brak kontrolera lub możliwości ustawienia akcji: " +
								this.params.controllerName +
								"." +
								elem.getAttribute("data-" + action)
							);
						}
						//elem.addEventListener(action, tests.catchEvent);
						elem.addEventListener(action as string, fn, false);
					}
				});
		});
	}

	render(template: string): Function {
		return handlebars.compile(template);
	}
}

/**
 * [[Class'a construktor obiektu ROUTER]]
 * @author Leszek
 * @returns {[[Type]]} [[Description]]
 */
class Router {
	//tests.debug(this);

	//prywatne
	//var self = this;
	start: boolean = true;
	routeChangeLocked: boolean = false;
	app: appLib;

	constructor(app: appLib) {
		this.app = app;
	}

	createGlobalViews(): void {
		this.globalViews.forEach((view: {}) => {
			this.app.createView(
				(<any>view).name,
				this.currentRoute!,
				(<any>view).params
			);
		});
	}

	//publiczne
	currentRoute?: string;
	routes: { [key: string]: RouteT } = {};
	globalViews: ViewT[] = [];

	setRouteChangeLocked(lock: boolean): void {
		this.routeChangeLocked = lock;
	}

	getRouteChangeLocked(): boolean {
		return this.routeChangeLocked;
	}

	showRoute(route: string): void {
		if (this.routeChangeLocked) {
			return;
		}

		//tests.groupEnd();
		//tests.debug('', 'route->' + state);
		//tests.groupEnd();

		let currroute: RouteT = this.routes[route];

		if (currroute == undefined) {
			console.error("Brak definicji widoku:" + route, "BŁĄD");
			currroute = this.routes["#start"];
		}

		this.currentRoute = (<any>currroute).name;

		(<any>currroute).views.forEach((view: View) => {
			//elem.view(elem.param);
			//view.params.route = this.currentRoute;
			this.app.createView(view.name, this.currentRoute!, view.params);
		});

		if (this.start) {
			this.createGlobalViews();
			this.start = false;
		}
		(<any>currroute).nav
			? (document.querySelector("nav")!.className = "nav")
			: (document.querySelector("nav")!.className = "hidden");
		(<any>currroute).footer
			? (document.querySelector("footer")!.className = "footer")
			: (document.querySelector("footer")!.className = "hidden");
		setTimeout(this.app.onRouteChange, 0);
		//location.hash="";
	}

	init(page: string): void {
		//window.addEventListener("hashchange", showRoute);
		this.showRoute(page);
	}
}

export let app: appLib = new appLib();
