const webpack = require("webpack");
const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const config = {
	entry: "./src/ts/index.ts",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "index.js",
	},

	module: {
		rules: [
			{
				test: /\.ts(x)?$/,
				loader: "ts-loader",
				exclude: /node_modules/,
			},
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"],
			},
			{
				test: /\.html$/i,
				loader: "html-loader",
				options: {
					minimize: false,
				},
			},
		],
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js"],
	},

	plugins: [
		new CleanWebpackPlugin(),
		new CopyPlugin({
			patterns: [
				{ from: "./src/index.html" },
				{ from: "./src/favicon.png" },
				{ from: "./src/manifest.json" },
				{ from: "./src/favicon.ico" },
				{ from: "./src/icons/", to: "icons/" },
			],
		}),
		new TerserPlugin({
			terserOptions: {
				compress: {
					drop_console: true,
					ecma: 2017,
					keep_fargs: true,
				},
				mangle: false,
				enclose: false,
				keep_classnames: false,
				keep_fnames: true,
			},
		}),
	],

	optimization: {
		minimize: true,
	},
};

module.exports = config;
